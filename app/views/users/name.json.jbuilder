# frozen_string_literal: true

json.user do
  json.first_name @user.first_name
  json.last_name @user.last_name
  json.full_name "#{@user.first_name} #{@user.last_name}"
end
