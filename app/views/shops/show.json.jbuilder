# frozen_string_literal: true

json.extract! @shop, :name, :user
json.url user_shop_url(@user, format: :json)
