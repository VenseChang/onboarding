# frozen_string_literal: true

class User
  include Mongoid::Document
  include Mongoid::Timestamps

  field :age,        type: Integer
  field :first_name, type: String
  field :gender,     type: String
  field :last_name,  type: String
  field :address,    type: Hash

  GENDERS = %w[male female others].freeze

  validates :first_name, :last_name, presence: true
  validates :age, numericality: { only_integer: true,
                                  greater_than_or_equal_to: 0 }
  validates :gender, inclusion: { in: GENDERS }, allow_blank: true

  has_one :shop

  def full_address
    address['address_2'] + ', ' + address['address_1'] + ', ' + address['country']
  end
end
