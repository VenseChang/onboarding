# frozen_string_literal: true

class Shop
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  validates :name, presence: true

  belongs_to :user
end
