# frozen_string_literal: true

class ShopsController < ApplicationController
  before_action :set_user
  before_action :set_shop, only: %i[show new create edit update destroy]

  def new; end

  def create
    @shop = Shop.new(shop_params)

    respond_to do |format|
      if @shop.save
        format.html { redirect_to @user, notice: 'Shop was successfully created.' }
        format.json { render :show, status: :created, location: @shop }
      else
        format.html { render :new }
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit; end

  def update
    respond_to do |format|
      if @shop.update(shop_params)
        format.html { redirect_to user_shop_path(@user), notice: 'Shop was successfully updated.' }
        format.json { render :show, status: :ok, location: @shop }
      else
        format.html { render :edit }
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  def show; end

  def destroy
    @shop.destroy
    respond_to do |format|
      format.html { redirect_to user_url(@user), notice: 'Shop was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_user
    @user = User.find(params[:user_id])
  end

  def set_shop
    @shop = @user.shop || Shop.new
  end

  def shop_params
    params.require(:shop)
          .permit(:name)
          .merge(user: @user)
  end
end
