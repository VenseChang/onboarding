# frozen_string_literal: true

Rails.application.routes.draw do
  resources :users do
    resource :shop

    member do
      get :name
    end

    collection do
      get :count
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'users#index'
end
