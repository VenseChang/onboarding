# frozen_string_literal: true

class AddAgeAndGenderToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :age, :integer
    add_column :users, :gender, :integer
  end
end
