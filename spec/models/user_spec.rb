# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#validations' do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it {
      is_expected.to validate_numericality_of(:age)
        .only_integer
        .is_greater_than_or_equal_to(0)
    }
    it {
      is_expected.to validate_inclusion_of(:gender)
        .in_array(%w[male female others])
        .allow_blank
    }
  end
end
